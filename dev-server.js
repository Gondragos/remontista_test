var fs = require('fs')

var babelrc = fs.readFileSync('./.babelrc')
var config

try {
  config = JSON.parse(babelrc)
} catch (err) {
  console.error('==>     ERROR: Error parsing your .babelrc.')
  console.error(err)
}

require('babel-core/register')(config)

const http = require('http')
const express = require('express')
const app = express()
const path = require('path')

;(function initWebpack () {
  const webpack = require('webpack')
  const webpackConfig = require('./webpack.config')
  const compiler = webpack(webpackConfig)

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true, publicPath: webpackConfig.output.publicPath
  }))

  app.use(require('webpack-hot-middleware')(compiler, {
    log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
  }))

  app.use(express.static(path.join(__dirname, '/')))
})()

app.get(/.*/, function root (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'))
})

const server = http.createServer(app)

server.listen(process.env.PORT || 3000, process.env.ADDR || '', function onListen () {
  const address = server.address()
  console.log('Listening on: %j', address)
  console.log(' -> that probably means: http://localhost:%d', address.port)
})
