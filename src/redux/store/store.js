import { applyMiddleware, compose, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import { rootReducer } from '../reducers'

export default function configureStore () {
  let store
  if (process.env.NODE_ENV === 'development') {
    store = compose(
      applyMiddleware(
        createLogger()
      )
    )(createStore)(rootReducer)
  } else {
    store = compose(applyMiddleware())(createStore)(rootReducer)
  }

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').rootReducer
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
