import { SET_SELECTED } from '../constants/list'

const initialState = {
  /*
  * Хотелось бы хранить в виде items: [ { name: "option 1", selected: <true|false> } ],
  * но в задании ограничение "дан список строк"
  */
  selected: [],
  // На данном этапе список предполагается статичным
  items: [
    'Москва',
    'Санкт-Петербург',
    'Новосибирск',
    'Екатеринбург',
    'Нижний Новгород',
    'Казань',
    'Челябинск',
    'Омск',
    'Самара',
    'Ростов-на-Дону',
    'Уфа',
    'Красноярск',
    'Пермь',
    'Воронеж',
    'Волгоград'
  ]
}

export default function App (state = initialState, action) {
  switch (action.type) {
    case SET_SELECTED:
      return {...state, selected: [action.payload]}
    default:
      return state
  }
}
