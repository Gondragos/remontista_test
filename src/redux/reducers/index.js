import { combineReducers } from 'redux'
import List from './list'

export const rootReducer = combineReducers({
  List
})
