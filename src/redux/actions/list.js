import {
  SET_SELECTED
} from '../constants/list'

export function setSelected (payload) {
  return {
    type: SET_SELECTED,
    payload
  }
}
