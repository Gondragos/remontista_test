import 'babel-polyfill'
import { AppContainer } from 'react-hot-loader'
import React from 'react'
import { render } from 'react-dom'
import './index.css'
import App from 'containers/App/App'
import { Provider } from 'react-redux'
import configureStore from 'redux/store/store'

const store = configureStore()

const renderApp = () => {
  render(
    <AppContainer warnings={false}>
      <Provider store={store}>
        <App />
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  )
}

renderApp()

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('containers/App/App', () => {
    renderApp()
  })
}
