import React, { Component } from 'react'
import './App.css'
import Block2 from './components/Block2/Block2'
import Block1 from './components/Block1/Block1'

export default class App extends Component {
  render () {
    return <div className='app'>
      <div className='app__block'><Block1 /></div>
      <div className='app__block'><Block2 /></div>
    </div>
  }
}
