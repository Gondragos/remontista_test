import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import List from 'components/List/List'

function mapStateToProps (state) {
  return {
    items: state.List.items,
    selected: state.List.selected
  }
}

@connect(mapStateToProps)
export default class Block2 extends PureComponent {
  render () {
    const {items, selected} = this.props
    return <List
      items={items}
      selected={selected}
    />
  }
}
