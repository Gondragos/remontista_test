import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Select from 'components/Select/Select'
import * as ListActions from 'redux/actions/list'
import { bindActionCreators } from 'redux'

function mapStateToProps (state) {
  return {
    items: state.List.items,
    selected: state.List.selected
  }
}

function mapDispatchToProps (dispatch) {
  return {
    ListActions: bindActionCreators(ListActions, dispatch)
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Block1 extends PureComponent {
  generateOptions () {
    const {items} = this.props
    return items.map((item, idx) => {
      return {
        value: idx,
        label: item
      }
    })
  }

  render () {
    const {selected, ListActions} = this.props
    return <Select
      value={selected[0]}
      options={this.generateOptions()}
      onChange={(value) => {
        ListActions.setSelected(value)
      }}
    />
  }
}
