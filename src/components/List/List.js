import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import './List.css'

/**
 * Компонент, отображающий список строк.
 * Т.к. компонент уникален для данной задачи - интерфейс произвольный
 */
export default class List extends PureComponent {
  static propTypes = {
    // список элементов
    items: PropTypes.arrayOf(PropTypes.string),
    // список индексов выбранных элементов
    selected: PropTypes.arrayOf(PropTypes.number)
  }

  baseCls = 'list'
  elementCls = 'list__item'
  selectedElementCls = 'list__item_selected'

  renderItem (item, idx) {
    const {elementCls, selectedElementCls} = this
    const {selected} = this.props
    return <li
      key={idx}
      className={classNames(
        elementCls,
        selected.indexOf(idx) > -1 ? selectedElementCls : null
      )}
    >
      {item}
    </li>
  }

  render () {
    const {baseCls} = this
    const {items} = this.props
    return <ul className={baseCls}>
      {items.map(this.renderItem.bind(this))}
    </ul>
  }
}
