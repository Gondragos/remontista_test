import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import './Select.css'
import classNames from 'classnames'

const KEY_CODE_ENTER = 13
const KEY_CODE_UP = 38
const KEY_CODE_DOWN = 40
const KEY_CODE_ESC = 27

/**
 * Компонент выбора с выпадающим списком.
 * Интерфейс частично совместим с react-select для возможности апгрейда в случае необходимости
 */
export default class Select extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    options: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]),
      label: PropTypes.string
    })),
    onChange: PropTypes.func
  }
  static defaultProps = {
    value: '',
    onChange: () => {}
  }

  baseCls = 'select'
  baseExpandedCls = this.baseCls + '_expanded'
  displayCls = this.baseCls + '__display'
  displayExpandedCls = this.displayCls + '_expanded'
  optionsCls = this.baseCls + '__options'
  optionsExpandedCls = this.optionsCls + '_expanded'
  optionCls = this.baseCls + '__option'
  optionTargeredCls = this.optionCls + '_targeted'

  constructor () {
    super()
    this.state = {
      expanded: false,
      target: 0
    }

    const autobindMethods = [
      'handleClickOutside',
      'handleSelectKeyDown',
      'handleDisplayClick'
    ]
    autobindMethods.forEach((method) => { this[method] = this[method].bind(this) })
  }

  componentWillMount () {
    this.updateValueIndex(this.props)
  }

  componentDidMount () {
    document.addEventListener('touchstart', this.handleClickOutside)
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('touchstart', this.handleClickOutside)
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUpdate (nextProps) {
    this.updateValueIndex(nextProps)
  }

  componentDidUpdate (_, nextState) {
    const {expanded} = this.state

    if (nextState.expanded !== expanded && !expanded) {
      this.focusMainElement()
    }
  }

  updateValueIndex (props) {
    const {value, options} = props
    this.valueIdx = options.findIndex((option) => {
      return option.value === value
    })
  }

  focusMainElement () {
    const {element} = this
    element && element.focus()
  }

  handleClickOutside (e) {
    if (this.wrapper && !this.wrapper.contains(e.target)) {
      this.toggleExpanded(false)
    }
  }

  handleSelectKeyDown (e) {
    e.preventDefault()
    switch (e.keyCode) {
      case KEY_CODE_ENTER:
        this.onEnter()
        break
      case KEY_CODE_UP:
        this.prevTarget()
        break
      case KEY_CODE_DOWN:
        this.nextTarget()
        break
      case KEY_CODE_ESC:
        this.toggleExpanded(false)
        break
    }
  }

  handleDisplayClick () {
    this.toggleExpanded()
  }

  handleOptionClick (option) {
    this.chooseOption(option)
  }

  onEnter () {
    const {expanded} = this.state
    if (expanded) {
      this.chooseTargeted()
    } else {
      this.toggleExpanded()
    }
  }

  chooseTargeted () {
    const {target} = this.state
    const {options} = this.props
    const option = options[target]
    this.chooseOption(option)
  }

  prevTarget () {
    this.incTarget(-1)
  }

  nextTarget () {
    this.incTarget(1)
  }

  incTarget (step) {
    const {options} = this.props
    const {target, expanded} = this.state
    const size = options.length
    if (expanded) {
      const newTarget = target + step
      this.setState({
        target: (newTarget + size) % size
      })
    } else {
      this.toggleExpanded(true)
    }
  }

  chooseOption (option) {
    const {onChange} = this.props
    this.toggleExpanded(false)
    onChange(option.value)
  }

  toggleExpanded (shallExpand) {
    const {expanded} = this.state
    const {valueIdx} = this
    this.setState({
      expanded: typeof shallExpand === 'undefined' ? !expanded : shallExpand,
      target: valueIdx
    })
  }

  renderDisplay () {
    const {expanded} = this.state
    const {options} = this.props
    const {displayCls, displayExpandedCls, valueIdx} = this
    const option = options[valueIdx]
    const label = option ? option.label : ''
    return <div
      tabIndex='0'
      ref={ref => { this.element = ref }}
      className={classNames(
        displayCls,
        expanded ? displayExpandedCls : null
      )}
      onClick={this.handleDisplayClick}
    >
      {label}
    </div>
  }

  renderOptions () {
    const {optionsCls, optionsExpandedCls} = this
    const {expanded} = this.state
    const {options} = this.props
    return <div className={classNames(
      optionsCls,
      expanded ? optionsExpandedCls : null
    )}>
      {options.map((option, idx) => { return this.renderOption(option, idx) })}
    </div>
  }

  renderOption (option, idx) {
    const {optionCls, optionTargeredCls} = this
    const {target} = this.state
    const isTarget = target === idx
    return <div
      key={option.value}
      ref={isTarget ? (ref) => { ref && ref.focus() } : null}
      className={classNames(
        optionCls,
        isTarget ? optionTargeredCls : null
      )}
      tabIndex={isTarget ? -1 : null}
      onClick={this.handleOptionClick.bind(this, option)}
    >
      {option.label}
    </div>
  }

  render () {
    const {expanded} = this.state
    const {name, value} = this.props
    const {baseCls, baseExpandedCls} = this
    return <div
      ref={ref => { this.wrapper = ref }}
      className={classNames(
        baseCls,
        expanded ? baseExpandedCls : null
      )}
      onKeyDown={this.handleSelectKeyDown}
    >
      {name && <input type='hidden' name={name} value={value} />}
      {this.renderDisplay()}
      {this.renderOptions()}
    </div>
  }
}
